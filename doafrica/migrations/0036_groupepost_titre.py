# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doafrica', '0035_message'),
    ]

    operations = [
        migrations.AddField(
            model_name='groupepost',
            name='titre',
            field=models.CharField(max_length=256, blank=True),
        ),
    ]
