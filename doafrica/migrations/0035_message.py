# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('doafrica', '0034_delete_utilisateur'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object', models.CharField(max_length=256, blank=True)),
                ('texte', models.TextField(blank=True)),
                ('image', models.FileField(upload_to=b'', blank=True)),
                ('sendtime', models.DateField(auto_now_add=True)),
                ('read', models.CharField(default=b'non', max_length=3)),
                ('receiver', models.ForeignKey(related_name='receiver', to=settings.AUTH_USER_MODEL)),
                ('sender', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
