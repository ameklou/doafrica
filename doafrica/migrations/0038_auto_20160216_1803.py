# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doafrica', '0037_auto_20160216_0747'),
    ]

    operations = [
        migrations.RenameField(
            model_name='localpost',
            old_name='image',
            new_name='photo',
        ),
    ]
