# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doafrica', '0033_auto_20160201_1118'),
    ]

    operations = [
        migrations.DeleteModel(
            name='utilisateur',
        ),
    ]
