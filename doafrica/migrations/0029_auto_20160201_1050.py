# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('doafrica', '0028_auto_20160201_1041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='date_de_pub',
            field=models.DateField(default=datetime.datetime(2016, 2, 1, 10, 50, 40, 261444, tzinfo=utc)),
        ),
    ]
