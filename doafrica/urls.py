from django.conf.urls import url


from . import views

urlpatterns= [
	url(r'^$', views.index, name='index'),
	url(r'^signup$', views.signup, name='signup'),
	url(r'^conlogin$', views.conlogin, name='conlogin'),
	url(r'^profil$', views.profil, name='profil'),
	url(r'^conlogout$', views.conlogout, name='conlogout'),
	url(r'^edit_profil$', views.edit_profil, name='edit_profil'),
	url(r'^photo$', views.photo, name='photo'),
    url(r'^association$',views.association, name='association'),
    url(r'^admin$',views.admin, name='admin'),
	url(r'^inbox$',views.inbox, name='inbox'),
    url(r'^sent$',views.sent, name='sent'),
	url(r'^locale$',views.locale, name='locale'),
	url(r'^premium$',views.premium, name='premium'),
    url(r'^suscribe$',views.suscribe, name='suscribe'),
    url(r'^suscribes$',views.suscribes, name='suscribes'),
    url(r'^reponse/(\d+)$',views.reponse, name='reponse'),
	url(r'^projet/(\d+)$',views.projet, name='projet'),
	url(r'^message$',views.message, name='message'),
	url(r'^list_projet$',views.list_projet, name='list_projet'),
	url(r'^groupe/(\d+)$',views.groupe, name='groupe'),
    url(r'^settings/(\d+)$',views.settings, name='settings'),
    url(r'^adminlist$',views.adminlist, name='adminlist'),
    url(r'^suspendre/(\d+)$',views.suspendre, name='suspendre'),
    url(r'^valider/(\d+)$',views.valider, name='valider'),
	url(r'^util/(\d+)$',views.util, name='util'),
	url(r'^adduser$',views.adduser, name='adduser'),
    url(r'^notification$',views.notification, name='notification'),
    url(r'^delete/(\d+)$',views.delete, name='delete'),
	url(r'^user$',views.user, name='user'),
	url(r'^erreurlog$',views.erreurlog, name='erreurlog'),
    url(r'^voir/(\d+)$',views.voir, name='voir'),
    url(r'^active/(\d+)$',views.active, name='active'),
	url(r'^globale$',views.globale, name='globale'),
	url(r'^invest$',views.invest, name='invest'),
    url(r'^cabinet$',views.cabinet, name='cabinet'),
    url(r'^follow/(\d+)$',views.follow, name='follow'),
    url(r'^listfollow$',views.listfollow, name='listfollow'),
	url(r'^addfriend/(\d+)$',views.addfriend, name='addfriend'),
	 url(r'^ajax/chat$',views.ajax_chat, name='ajax_chat'),



]