from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.



class Profil(User):
    """ model utilisateurs"""
    account_choice=(
        ('A','Entrepreneur'),
        ('B','Cabinet'),
        ('C','Investisseur'),
        )
    premium_choice=(
        ('A','Free'),
        ('B','Argent'),
        ('C','Gold'),
        )
    user= models.OneToOneField(User)
    telephone=models.IntegerField(null=True)
    country=models.CharField(max_length=50)
    address=models.CharField(max_length=50)
    biography=models.TextField()
    slug=models.SlugField(max_length=100)
    twitter=models.CharField(max_length=255)
    facebook=models.CharField(max_length=255)
    linkedin=models.CharField(max_length=255)
    photo=models.ImageField(upload_to="photo/")
    account=models.CharField(max_length=1, choices=account_choice)
    activated=models.CharField(max_length=5, default="non")
    active=models.CharField(max_length=5, null=True)
    premium=models.CharField(max_length=1, choices=premium_choice)
    register_date=models.DateField(auto_now_add=True)
    id_card=models.ImageField(upload_to="photo/")
    business_card=models.ImageField(upload_to="photo/")

    def __str__(self):
        return self.user.username





class Post(models.Model):
    """ model pour la publication de """
    secteur_choice=(
        ('A','commercial'),
        ('B','agricole'),
        ('C','service'),
        ('D','social'),
        )
    author=models.ForeignKey(User)
    titre=models.CharField(max_length=255)
    message=models.TextField()
    date_de_pub=models.DateField(auto_now_add=True)
    montant=models.IntegerField(blank=True, null=True)
    categorie=models.CharField(max_length=1, choices=secteur_choice, blank=True)
    image=models.ImageField(upload_to="projet", blank=True)
    recolte=models.IntegerField(null=True)
    valide=models.CharField(max_length=5, null=True)


class Document(models.Model):
    docfile = models.FileField(upload_to='documents/')

class Groupe(models.Model):
    """ Model gerant les groupes d'utilisateurs"""
    name=models.CharField(max_length=255)
    description=models.TextField()
    author=models.ForeignKey(User)
    image=models.ImageField(upload_to="groupe")

class GroupePost(models.Model):
    """Model gerant les publications ds les groupes """
    titre=models.CharField(max_length=256, blank=True)
    groupe=models.ForeignKey(Groupe)
    message=models.TextField()
    image=models.ImageField()
    publication=models.DateField(auto_now_add=True)
    author=models.ForeignKey(User)


class GroupeMember(models.Model):
    """ model gerant les membres d'un groupe
    """
    membre=models.ForeignKey(User)
    groupe=models.ForeignKey(Groupe)


class Message(models.Model):
    """ model gerant les messages"""
    sender=models.ForeignKey(User)
    receiver=models.ForeignKey(User, related_name="receiver")
    object=models.CharField(max_length=256, blank=True)
    texte=models.TextField(blank=True)
    image=models.FileField(blank=True, upload_to="message")
    sendtime=models.DateField(auto_now_add=True)
    read=models.CharField(max_length=3, default="non")

class GlobalPost(models.Model):
    sendtime=models.DateField(auto_now_add=True)
    message=models.TextField(blank=True)
    image=models.FileField(blank=True, upload_to="adminpost")
    title=models.CharField(max_length=256, blank=True)


class LocalPost(models.Model):
    sendtime=models.DateField(auto_now_add=True)
    texte=models.TextField(blank=True)
    photo=models.FileField(blank=True, upload_to="adminpost")
    titre=models.CharField(max_length=256, blank=True)
    location=models.CharField(max_length=256)

class Notification(models.Model):
    de=models.ForeignKey(User)
    to=models.ForeignKey(User, related_name="receveur")
    sendtime=models.DateField(auto_now_add=True)
    read=models.CharField(max_length=4, blank=True)
    text=models.TextField()


class Friendship(models.Model):
    de=models.OneToOneField(User, related_name="sender")
    to=models.ForeignKey(User)
    sendtime=models.DateField(auto_now_add=True)
    answer=models.CharField(max_length=4, blank=True)

class Follow(models.Model):
    """ model des projets suivi par un cabinet"""
    de=models.ForeignKey(User)
    project=models.ForeignKey(Post)
    followtime=models.DateField(auto_now_add=True)

class Donate(models.Model):
    de=models.ForeignKey(User)
    project=models.ForeignKey(Post)
    montant=models.IntegerField(blank=True, null=True,)
    donatetime=models.DateField(auto_now_add=True)


class Investir(models.Model):
    de=models.ForeignKey(User)
    project=models.ForeignKey(Post)
    montant=models.IntegerField(blank=True, null=True)
    donatetime=models.DateField(auto_now_add=True)



